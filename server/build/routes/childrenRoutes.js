"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const childrenController_1 = __importDefault(require("../controllers/childrenController"));
class ChildrenRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', childrenController_1.default.list);
        this.router.get('/:id', childrenController_1.default.listByID);
        this.router.post('/', childrenController_1.default.create);
        this.router.put('/:id', childrenController_1.default.update);
        this.router.delete('/:id', childrenController_1.default.delete);
    }
}
const childrenRoutes = new ChildrenRoutes();
exports.default = childrenRoutes.router;
