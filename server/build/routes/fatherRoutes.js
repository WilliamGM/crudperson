"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const fatherController_1 = __importDefault(require("../controllers/fatherController"));
class FatherRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', fatherController_1.default.list);
        this.router.get('/:id', fatherController_1.default.listByID);
        this.router.post('/', fatherController_1.default.create);
        this.router.put('/:id', fatherController_1.default.update);
        this.router.delete('/:id', fatherController_1.default.delete);
    }
}
const fatherRoutes = new FatherRoutes();
exports.default = fatherRoutes.router;
