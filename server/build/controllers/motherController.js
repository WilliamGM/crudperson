"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const Mother_1 = require("../class/Mother");
class MotherController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield database_1.pool.query('SELECT * FROM mother');
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    listByID(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('SELECT * FROM mother WHERE idmother = $1', [id]);
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { full_name, birth } = req.body;
                const mother = new Mother_1.Mother(req.body.full_name, req.body.birth);
                const response = yield database_1.pool.query('INSERT INTO mother (full_name, birth) VALUES ($1, $2)', [mother.full_name, mother.birth]);
                console.log(response);
                res.json({
                    message: 'mother Added Succesfully',
                    body: {
                        user: { full_name, birth }
                    }
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const { full_name, birth } = req.body;
                const mother = new Mother_1.Mother(req.body.full_name, req.body.birth);
                const response = yield database_1.pool.query('UPDATE mother SET full_name = $1, birth = $2 WHERE idmother = $3 ', [
                    mother.full_name,
                    mother.birth,
                    id
                ]);
                res.json('mother Update');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('DELETE FROM mother WHERE idmother = $1', [id]);
                res.json('mother deleted successfully');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
}
const motherController = new MotherController();
exports.default = motherController;
