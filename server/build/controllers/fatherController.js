"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const Father_1 = require("../class/Father");
class FatherController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield database_1.pool.query('SELECT * FROM father');
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    listByID(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('SELECT * FROM father WHERE idfather = $1', [id]);
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { full_name, birth } = req.body;
                const father = new Father_1.Father(req.body.full_name, req.body.birth);
                const response = yield database_1.pool.query('INSERT INTO father (full_name, birth) VALUES ($1, $2)', [father.full_name, father.birth]);
                console.log(response);
                res.json({
                    message: 'User Added Succesfully',
                    body: {
                        user: { full_name, birth }
                    }
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const { full_name, birth } = req.body;
                const father = new Father_1.Father(req.body.full_name, req.body.birth);
                const response = yield database_1.pool.query('UPDATE father SET full_name = $1, birth = $2 WHERE idfather = $3 ', [
                    father.full_name,
                    father.birth,
                    id
                ]);
                res.json('Father Update');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('DELETE FROM father WHERE idfather = $1', [id]);
                res.json('father deleted successfully');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
}
const fatherController = new FatherController();
exports.default = fatherController;
