"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const Children_1 = require("../class/Children");
class ChildrenController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield database_1.pool.query('SELECT * FROM children');
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    listByID(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('SELECT * FROM children WHERE idchildren = $1', [id]);
                res.status(200).json(response.rows);
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { full_name, birth, father, mother } = req.body;
                const children = new Children_1.Children(req.body.full_name, req.body.birth, req.body.father, req.body.mother);
                const response = yield database_1.pool.query('INSERT INTO children (full_name, birth, mother, father) VALUES ($1, $2, $3, $4)', [children.full_name, children.birth, children.father, children.mother]);
                console.log(response);
                res.json({
                    message: 'User Added Succesfully',
                    body: {
                        user: { full_name, birth, mother, father }
                    }
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const { full_name, birth, father, mother } = req.body;
                const children = new Children_1.Children(req.body.full_name, req.body.birth, req.body.father, req.body.mother);
                const response = yield database_1.pool.query('UPDATE children SET full_name = $1, birth = $2, father = $3, mother = $4 WHERE idchildren = $5 ', [
                    children.full_name,
                    children.birth,
                    children.father,
                    children.mother,
                    id
                ]);
                res.json('User Update');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const response = yield database_1.pool.query('DELETE FROM children WHERE idchildren = $1', [id]);
                res.json('User deleted successfully');
            }
            catch (e) {
                console.log(e);
            }
        });
    }
}
const childrenController = new ChildrenController();
exports.default = childrenController;
