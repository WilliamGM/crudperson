"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Children = void 0;
const Person_1 = require("./Person");
class Children extends Person_1.Person {
    constructor(fullName, birth, father, mother) {
        super(fullName, birth);
        this.father = father;
        this.mother = mother;
    }
}
exports.Children = Children;
