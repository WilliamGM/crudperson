"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mother = void 0;
const Person_1 = require("./Person");
class Mother extends Person_1.Person {
    constructor(full_name, birth) {
        super(full_name, birth);
    }
}
exports.Mother = Mother;
