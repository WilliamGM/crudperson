"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Father = void 0;
const Person_1 = require("./Person");
class Father extends Person_1.Person {
    constructor(full_name, birth) {
        super(full_name, birth);
    }
}
exports.Father = Father;
