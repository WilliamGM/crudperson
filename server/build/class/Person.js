"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Person = void 0;
class Person {
    constructor(full_name, birth) {
        this.full_name = full_name;
        this.birth = birth;
    }
}
exports.Person = Person;
