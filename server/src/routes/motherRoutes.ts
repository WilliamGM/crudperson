import { Router } from 'express';

import motherController from '../controllers/motherController';

class MotherRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/',motherController.list);
        this.router.get('/:id',motherController.listByID);
        this.router.post('/',motherController.create);
        this.router.put('/:id',motherController.update);
        this.router.delete('/:id',motherController.delete);
    }
}

const motherRoutes = new MotherRoutes();
export default motherRoutes.router;