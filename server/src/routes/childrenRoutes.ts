import { Router } from 'express';

import childrenController from '../controllers/childrenController';

class ChildrenRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/',childrenController.list);
        this.router.get('/:id',childrenController.listByID);
        this.router.post('/',childrenController.create);
        this.router.put('/:id',childrenController.update);
        this.router.delete('/:id',childrenController.delete);
    }
}

const childrenRoutes = new ChildrenRoutes();
export default childrenRoutes.router;