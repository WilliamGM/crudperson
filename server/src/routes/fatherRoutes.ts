import { Router } from 'express';

import fatherController from '../controllers/fatherController';

class FatherRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/',fatherController.list);
        this.router.get('/:id',fatherController.listByID);
        this.router.post('/',fatherController.create);
        this.router.put('/:id',fatherController.update);
        this.router.delete('/:id',fatherController.delete);
    }
}

const fatherRoutes = new FatherRoutes();
export default fatherRoutes.router;