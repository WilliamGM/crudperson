import {Person} from "./Person";

export class Father extends Person{
    constructor(full_name:string,birth:Date){
        super(full_name, birth);
    }
}
