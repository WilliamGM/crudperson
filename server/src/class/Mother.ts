import {Person} from "./Person";

export class Mother extends Person{
    constructor(full_name:string,birth:Date){
        super(full_name, birth);
    }
}
