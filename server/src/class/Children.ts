import {Person} from "./Person";

export class Children extends Person{
    public father:string;
    public mother:string;
    
    constructor(fullName:string, birth:Date, father:string, mother:string){
        super(fullName, birth);
        this.father = father;
        this.mother = mother;
    }
}