export class Person{
    public full_name: string;
    public birth: Date;

    constructor(full_name:string,birth:Date){
        this.full_name = full_name;
        this.birth = birth;
    }
}
