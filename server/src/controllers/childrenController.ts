import {Request, Response} from 'express';

import {pool} from '../database';
import { QueryResult } from 'pg';
import {Children} from "../class/Children";

class ChildrenController{
    public async list (req: Request, res: Response): Promise<void> {
        try{
        const response: QueryResult = await pool.query('SELECT * FROM children');
        res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async listByID (req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('SELECT * FROM children WHERE idchildren = $1', [id]);
            res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async create (req: Request, res: Response): Promise<void>{
        try{
            const {full_name, birth, father, mother}= req.body;
            const children = new Children(req.body.full_name, req.body.birth, req.body.father, req.body.mother);
            const response: QueryResult = await pool.query('INSERT INTO children (full_name, birth, mother, father) VALUES ($1, $2, $3, $4)', [children.full_name, children.birth, children.father, children.mother]);
            console.log(response);
            res.json({
                message: 'User Added Succesfully',
                body: {
                user: {full_name, birth, mother, father}
        }
    })
        }catch(e){
            console.log(e);
        }
    }

    public async update(req: Request, res: Response): Promise<void>{
        try{
            const id = req.params.id;
            const {full_name, birth, father, mother}= req.body;
            const children = new Children(req.body.full_name, req.body.birth, req.body.father, req.body.mother);
            const response: QueryResult = await pool.query('UPDATE children SET full_name = $1, birth = $2, father = $3, mother = $4 WHERE idchildren = $5 ', [
                children.full_name, 
                children.birth, 
                children.father, 
                children.mother,
                id
            ]);
            res.json('User Update');
        }catch(e){
            console.log(e);
        }
    }

    public async delete(req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('DELETE FROM children WHERE idchildren = $1', [id]);
            res.json('User deleted successfully');
        }catch(e){
            console.log(e);
        }
    }
}

const childrenController = new ChildrenController();
export default childrenController;