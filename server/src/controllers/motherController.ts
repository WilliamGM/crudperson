import {Request, Response} from 'express';

import {pool} from '../database';
import { QueryResult } from 'pg';
import {Mother} from "../class/Mother";

class MotherController{
    public async list (req: Request, res: Response): Promise<void> {
        try{
        const response: QueryResult = await pool.query('SELECT * FROM mother');
        res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async listByID (req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('SELECT * FROM mother WHERE idmother = $1', [id]);
            res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async create (req: Request, res: Response): Promise<void>{
        try{
            const {full_name, birth}= req.body;
            const mother = new Mother(req.body.full_name, req.body.birth);
            const response: QueryResult = await pool.query('INSERT INTO mother (full_name, birth) VALUES ($1, $2)', [mother.full_name, mother.birth]);
            console.log(response);
            res.json({
                message: 'mother Added Succesfully',
                body: {
                user: {full_name, birth}
        }
    })
        }catch(e){
            console.log(e);
        }
    }

    public async update(req: Request, res: Response): Promise<void>{
        try{
            const id = req.params.id;
            const {full_name, birth}= req.body;
            const mother = new Mother(req.body.full_name, req.body.birth);
            const response: QueryResult = await pool.query('UPDATE mother SET full_name = $1, birth = $2 WHERE idmother = $3 ', [
                mother.full_name, 
                mother.birth, 
                id
            ]);
            res.json('mother Update');
        }catch(e){
            console.log(e);
        }
    }

    public async delete(req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('DELETE FROM mother WHERE idmother = $1', [id]);
            res.json('mother deleted successfully');
        }catch(e){
            console.log(e);
        }
    }
}

const motherController = new MotherController();
export default motherController;