import {Request, Response} from 'express';

import {pool} from '../database';
import { QueryResult } from 'pg';
import {Father} from "../class/Father";

class FatherController{
    public async list (req: Request, res: Response): Promise<void> {
        try{
        const response: QueryResult = await pool.query('SELECT * FROM father');
        res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async listByID (req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('SELECT * FROM father WHERE idfather = $1', [id]);
            res.status(200).json(response.rows);
        }catch(e){
            console.log(e);
        }
    }

    public async create (req: Request, res: Response): Promise<void>{
        try{
            const {full_name, birth}= req.body;
            const father = new Father(req.body.full_name, req.body.birth);
            const response: QueryResult = await pool.query('INSERT INTO father (full_name, birth) VALUES ($1, $2)', [father.full_name, father.birth]);
            console.log(response);
            res.json({
                message: 'User Added Succesfully',
                body: {
                user: {full_name, birth}
        }
    })
        }catch(e){
            console.log(e);
        }
    }

    public async update(req: Request, res: Response): Promise<void>{
        try{
            const id = req.params.id;
            const {full_name, birth}= req.body;
            const father = new Father(req.body.full_name, req.body.birth);
            const response: QueryResult = await pool.query('UPDATE father SET full_name = $1, birth = $2 WHERE idfather = $3 ', [
                father.full_name, 
                father.birth, 
                id
            ]);
            res.json('Father Update');
        }catch(e){
            console.log(e);
        }
    }

    public async delete(req: Request, res: Response): Promise<void>{
        try{
            const id = parseInt(req.params.id);
            const response: QueryResult = await pool.query('DELETE FROM father WHERE idfather = $1', [id]);
            res.json('father deleted successfully');
        }catch(e){
            console.log(e);
        }
    }
}

const fatherController = new FatherController();
export default fatherController;