CREATE DATABASE person;

CREATE TABLE children (
	id SERIAL PRIMARY KEY,
    full_name VARCHAR (40),
    birth DATE,
	father INT,
	mother INT
);

CREATE TABLE father (
	id SERIAL PRIMARY KEY,
    full_name VARCHAR (40),
    birth DATE
);

CREATE TABLE mother (
	id SERIAL PRIMARY KEY,
    full_name VARCHAR (40),
    birth DATE
);

//Agregamos las llaves foraneas
ALTER TABLE children
ADD CONSTRAINT fkfather
FOREIGN KEY (father)
REFERENCES father(idfather);

ALTER TABLE children
ADD CONSTRAINT fkmother
FOREIGN KEY (mother)
REFERENCES mother(idmother);

