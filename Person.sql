PGDMP     5    ,                x            person    12.3    12.3                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    41031    person    DATABASE     �   CREATE DATABASE person WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE person;
                postgres    false            �            1259    41034    children    TABLE     �   CREATE TABLE public.children (
    idchildren integer NOT NULL,
    full_name character varying(40),
    birth date,
    father integer,
    mother integer
);
    DROP TABLE public.children;
       public         heap    postgres    false            �            1259    41032    children_idchildren_seq    SEQUENCE     �   CREATE SEQUENCE public.children_idchildren_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.children_idchildren_seq;
       public          postgres    false    203                        0    0    children_idchildren_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.children_idchildren_seq OWNED BY public.children.idchildren;
          public          postgres    false    202            �            1259    41042    father    TABLE     s   CREATE TABLE public.father (
    idfather integer NOT NULL,
    full_name character varying(40),
    birth date
);
    DROP TABLE public.father;
       public         heap    postgres    false            �            1259    41040    father_idfather_seq    SEQUENCE     �   CREATE SEQUENCE public.father_idfather_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.father_idfather_seq;
       public          postgres    false    205            !           0    0    father_idfather_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.father_idfather_seq OWNED BY public.father.idfather;
          public          postgres    false    204            �            1259    41050    mother    TABLE     s   CREATE TABLE public.mother (
    idmother integer NOT NULL,
    full_name character varying(40),
    birth date
);
    DROP TABLE public.mother;
       public         heap    postgres    false            �            1259    41048    mother_idmother_seq    SEQUENCE     �   CREATE SEQUENCE public.mother_idmother_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.mother_idmother_seq;
       public          postgres    false    207            "           0    0    mother_idmother_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.mother_idmother_seq OWNED BY public.mother.idmother;
          public          postgres    false    206            �
           2604    41037    children idchildren    DEFAULT     z   ALTER TABLE ONLY public.children ALTER COLUMN idchildren SET DEFAULT nextval('public.children_idchildren_seq'::regclass);
 B   ALTER TABLE public.children ALTER COLUMN idchildren DROP DEFAULT;
       public          postgres    false    202    203    203            �
           2604    41045    father idfather    DEFAULT     r   ALTER TABLE ONLY public.father ALTER COLUMN idfather SET DEFAULT nextval('public.father_idfather_seq'::regclass);
 >   ALTER TABLE public.father ALTER COLUMN idfather DROP DEFAULT;
       public          postgres    false    205    204    205            �
           2604    41053    mother idmother    DEFAULT     r   ALTER TABLE ONLY public.mother ALTER COLUMN idmother SET DEFAULT nextval('public.mother_idmother_seq'::regclass);
 >   ALTER TABLE public.mother ALTER COLUMN idmother DROP DEFAULT;
       public          postgres    false    207    206    207                      0    41034    children 
   TABLE DATA           P   COPY public.children (idchildren, full_name, birth, father, mother) FROM stdin;
    public          postgres    false    203   �                 0    41042    father 
   TABLE DATA           <   COPY public.father (idfather, full_name, birth) FROM stdin;
    public          postgres    false    205   �                 0    41050    mother 
   TABLE DATA           <   COPY public.mother (idmother, full_name, birth) FROM stdin;
    public          postgres    false    207   $       #           0    0    children_idchildren_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.children_idchildren_seq', 8, true);
          public          postgres    false    202            $           0    0    father_idfather_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.father_idfather_seq', 9, true);
          public          postgres    false    204            %           0    0    mother_idmother_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.mother_idmother_seq', 5, true);
          public          postgres    false    206            �
           2606    41039    children children_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.children
    ADD CONSTRAINT children_pkey PRIMARY KEY (idchildren);
 @   ALTER TABLE ONLY public.children DROP CONSTRAINT children_pkey;
       public            postgres    false    203            �
           2606    41047    father father_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.father
    ADD CONSTRAINT father_pkey PRIMARY KEY (idfather);
 <   ALTER TABLE ONLY public.father DROP CONSTRAINT father_pkey;
       public            postgres    false    205            �
           2606    41055    mother mother_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.mother
    ADD CONSTRAINT mother_pkey PRIMARY KEY (idmother);
 <   ALTER TABLE ONLY public.mother DROP CONSTRAINT mother_pkey;
       public            postgres    false    207            �
           2606    41056    children fkfather    FK CONSTRAINT     v   ALTER TABLE ONLY public.children
    ADD CONSTRAINT fkfather FOREIGN KEY (father) REFERENCES public.father(idfather);
 ;   ALTER TABLE ONLY public.children DROP CONSTRAINT fkfather;
       public          postgres    false    2705    203    205            �
           2606    41061    children fkmother    FK CONSTRAINT     v   ALTER TABLE ONLY public.children
    ADD CONSTRAINT fkmother FOREIGN KEY (mother) REFERENCES public.mother(idmother);
 ;   ALTER TABLE ONLY public.children DROP CONSTRAINT fkmother;
       public          postgres    false    203    207    2707               $   x�3�����L��4202�50"NCNC�=... q�         7   x�3���LJ,J��4202�50".NǢ��̔�bNCKsC]s]c�=... 8"�         2   x�3�L�����4202�50".N�Ģ�DNCK3K]s]C�=... ��	�     